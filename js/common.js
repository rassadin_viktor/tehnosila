$(document).ready(function(){

	$('.b-slider__single-item').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  // autoplay: true,
  	autoplaySpeed: 2000,
	});

	$('.b-slider__multiple-items--six').slick({
	  infinite: true,
	  slidesToShow: 6,
	  slidesToScroll: 1,

	  responsive: [
    {
      breakpoint: 601,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
        infinite: true,
      }
    },
    {
      breakpoint: 321,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
      }
    }
    ]
	});

	$('.b-slider__multiple-items--four').slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,

	  responsive: [
    {
      breakpoint: 601,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
      }
    },
    {
      breakpoint: 321,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
      }
    }
    ]		

	});
});